Source: trackballs
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Markus Koschany <apo@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 gettext,
 guile-3.0-dev,
 libsdl2-dev,
 libsdl2-image-dev,
 libsdl2-mixer-dev,
 libsdl2-ttf-dev,
 zlib1g-dev
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://trackballs.github.io/
Vcs-Git: https://salsa.debian.org/games-team/trackballs.git
Vcs-Browser: https://salsa.debian.org/games-team/trackballs

Package: trackballs
Architecture: any
Breaks:
 trackballs-data (<< 1.2.3-3)
Replaces:
 trackballs-data (<< 1.2.3-3)
Depends:
 trackballs-data,
 ${misc:Depends},
 ${shlibs:Depends}
Description: OpenGL-based game of steering a marble through a labyrinth
 Trackballs is a simple game similar to the classical game Marble Madness
 on the Amiga in the 80's. By steering a marble ball through a labyrinth
 filled with vicious hammers, pools of acid and other obstacles the
 player collects points. When the ball reaches the destination it continues
 at the next, more difficult level - unless the time runs out.
 .
 This game is not intended to be a replica of Marble Madness but rather
 inspired by it. The game is also highly configurable by using a
 scripting extension (Guile) and it provides a simple editor by which new
 levels can easily be created.

Package: trackballs-data
Architecture: all
Multi-Arch: foreign
Depends:
 fonts-freefont-ttf,
 ${misc:Depends}
Enhances:
 trackballs
Breaks:
 trackballs-music (<< 1.3.1-2)
Replaces:
 trackballs-music (<< 1.3.1-2)
Description: Data files for trackballs
 This package contains the necessary architecture-independent data files
 needed for running trackballs.
